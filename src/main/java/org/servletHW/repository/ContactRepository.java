package org.servletHW.repository;

import org.servletHW.us.Contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ContactRepository {
    Map<Long, Contact> contactMap = new HashMap<>();

    public ContactRepository() {
    }

    public List<Contact> findByName(String name, String userId) {
        return this.findByUserLogin(userId).stream().filter((c) ->
                c.getName().startsWith(name)).collect(Collectors.toList());
    }

    public List<Contact> findAll() {
        return new ArrayList<>(this.contactMap.values());
    }

    public List<Contact> findByValue(String part, String userId) {
        return this.findByUserLogin(userId).stream().filter((c) ->
                c.getRecords().stream().anyMatch((r) -> r.getValue().contains(part))).collect(Collectors.toList());
    }

    public List<Contact> findByUserLogin(String user) {
        List<Contact> collect = this.contactMap.values().stream().filter((c) ->
                        c.getUser().getLogin().equals(user))
                .collect(Collectors.toList());
        return collect;
    }

    public Contact save(Contact c) {
        this.contactMap.put(c.getId(), c);
        return c;
    }



}
