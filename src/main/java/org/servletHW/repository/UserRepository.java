package org.servletHW.repository;

import org.servletHW.us.User;
import java.util.*;

public class UserRepository {
    Map<String, User> userMap = new HashMap<>();

    public UserRepository() {
    }

    public Optional<User> findByLogin(String login) {
        return Optional.ofNullable(this.userMap.get(login));
    }

    public Optional<User> findByPassword(String password) {
        return Optional.ofNullable(this.userMap.get(password));
    }

    public User save(User user) {
        this.userMap.put(user.getLogin(), user);
        return user;
    }

    public List<User> findAll() {
        return new ArrayList<>(this.userMap.values());
    }
}
