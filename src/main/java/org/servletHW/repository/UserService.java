package org.servletHW.repository;

import org.servletHW.us.User;

import java.util.Optional;

public class UserService {
    private final UserRepository userRepository;


    public User register(User user) {
        user.setPassword(user.getPassword());
        return this.userRepository.save(user);
    }

    public Optional<User> getByLogin(String login) {
        return this.userRepository.findByLogin(login);
    }

    public Optional<User> getByPassword(String password) {
        return this.userRepository.findByPassword(password);
    }

    public UserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void addToken(String login, String token) {
        this.userRepository.findByLogin(login).get().setToken(token);
    }
}
