package org.servletHW.us;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class User {
    String login;
    String password;
    @JsonProperty("date_born")
    String dateBorn;
    String token;

    @Override
    public String toString() {
        return "{" +
                "login='" + login + '\'' +
                ", dateBorn='" + dateBorn + '\'' +
                '}';
    }
}
