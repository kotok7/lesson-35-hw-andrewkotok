package org.servletHW.controllers;

import org.servletHW.BRException;
import org.servletHW.repository.ContactRepository;
import org.servletHW.repository.UserRepository;
import org.servletHW.repository.UserService;
import org.servletHW.us.Contact;
import org.servletHW.us.ContactRecord;
import org.servletHW.us.ContactType;
import org.servletHW.us.User;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class ServletController {
    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = new UserService(userRepository);
    private final ContactRepository contactRepository = new ContactRepository();
    private String response = null;

    public void showAllUsers(HttpServletResponse resp) throws IOException {
        String response = "";
        if (!userRepository.findAll().isEmpty()) {
            response = "{\n" +
                    "\"status\":\"ok\",\n" +
                    "\"users\":" +
                    userRepository.findAll() + "\n}";
        } else {
            System.out.println("{\"error\":\"List of users is empty\",\"status\":\"error\"}");
        }
        resp.getWriter().write(response);
        resp.getWriter().flush();
    }

    public void register(HttpServletRequest req) {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String dateBorn = req.getParameter("dateBorn");
        try {
            if (login != null && password != null) {
                if (this.userService.getByLogin(login).isPresent()) {
                    throw new BRException("Incorrect login");
                } else if (Objects.isNull(dateBorn)) {
                    throw new BRException("Birth date is null or incorrect");
                } else {
                    User user = new User();
                    user.setPassword(password);
                    user.setLogin(login);
                    user.setDateBorn(dateBorn);
                    this.userService.register(user);
                    System.out.println(Collections.singletonMap("status", "ok"));
                }
            } else {
                throw new BRException("Login and password cannot be NULL");
            }
        } catch (BRException e) {
            throw new RuntimeException(e);
        }
    }

    public void login(HttpServletRequest req) {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        try {
            if (login != null && password != null) {
                if (!(this.userService.getByLogin(login).isPresent() ||
                        this.userService.getByPassword(password).isPresent())) {
                    throw new BRException("Login or password is incorrect");
                } else {
                    String token = String.valueOf(login.hashCode());
                    this.userService.addToken(login, token);
                    System.out.println(Collections.singletonMap("status", "ok"));
                }
            } else {
                throw new BRException("Login and password cannot be NULL");
            }
        } catch (BRException e) {
            throw new RuntimeException(e);
        }
    }

    public void contactAddInfo(HttpServletRequest req) {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String recordType = req.getParameter("type");
        String recordValue = req.getParameter("value");
        try {
            if (login != null && password != null) {
                if (!(this.userService.getByLogin(login).isPresent() ||
                        this.userService.getByPassword(password).isPresent() ||
                        userService.getByLogin(login).get().getToken().isEmpty())) {
                    throw new BRException("Login or password is incorrect");
                } else {
                    Contact contact = new Contact();
                    ContactRecord contactRecord = new ContactRecord();
                    List<ContactRecord> recordsList = new ArrayList<>();
                    contact.setName(login);
                    contact.setId((long) (new Random().nextInt(100 - 1) + 1));
                    if (recordType.toUpperCase().equals(ContactType.EMAIL.toString())) {
                        contactRecord.setType(ContactType.EMAIL);
                    } else {
                        contactRecord.setType(ContactType.PHONE);
                    }
                    contactRecord.setValue(recordValue);
                    recordsList.add(contactRecord);
                    contact.setRecords(recordsList);
                    contactRepository.save(contact);
                    System.out.println(Collections.singletonMap("status", "ok"));
                }
            } else {
                throw new BRException("Login and password cannot be NULL");
            }
        } catch (BRException e) {
            throw new RuntimeException(e);
        }
    }

    public void showAllContacts(HttpServletResponse resp) throws IOException {
        if (!contactRepository.findAll().isEmpty()) {
            response = "{\n" +
                    "\"status\":\"ok\",\n" +
                    "\"contacts\":" +
                    contactRepository.findAll() + "\n}";
        } else {
            System.out.println("{\"error\":\"List of contacts is empty\",\"status\":\"error\"}");
        }
        resp.getWriter().write(response);
        resp.getWriter().flush();
    }

    public void contactFind(HttpServletRequest req) {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String searchByName = req.getParameter("name");
        String searchByValue = req.getParameter("value");
        Long contactID = contactRepository.findAll().stream()
                .filter(u->u.getName().equals(login)).findFirst().get().getId();
        try {
            if (login != null && password != null) {
                if (!(this.userService.getByLogin(login).isPresent() ||
                        this.userService.getByPassword(password).isPresent() ||
                        !userService.getByLogin(login).get().getToken().isEmpty())) {
                    throw new BRException("Login or password is incorrect");
                } else {
                    List<Contact> result;
                    if (!searchByName.isEmpty()) {
                        result = contactRepository.findByName(searchByName, String.valueOf(contactID));
                    } else if (!searchByValue.isEmpty()) {
                        result = contactRepository.findByValue(searchByValue, String.valueOf(contactID));
                    } else throw new BRException("Specify at list one parameter");
                    System.out.println(Collections.singletonMap("status", "ok"));
                    System.out.println(result.isEmpty());
                }
            } else {
                throw new BRException("Login and password cannot be NULL");
            }
        } catch (BRException e) {
            throw new RuntimeException(e);
        }
    }
}
