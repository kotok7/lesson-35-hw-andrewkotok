package org.servletHW.controllers;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class Servlet extends HttpServlet {
    ServletController servletController = new ServletController();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (req.getRequestURI().matches("(.*)/users(.*)")) {
            servletController.showAllUsers(resp);
        }
        if (req.getRequestURI().matches("(.*)/contacts")) {
            servletController.showAllContacts(resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getRequestURI().matches("(.*)/register")) {
            servletController.register(req);
        } else if (req.getRequestURI().matches("(.*)/login")) {
            servletController.login(req);
        } else if (req.getRequestURI().matches("(.*)/add")) {
            servletController.contactAddInfo(req);
        } else if (req.getRequestURI().matches("(.*)/contacts/find")) {
            servletController.contactFind(req);
        }
    }
}

