package org.servletHW;

public class BRException extends RuntimeException {

    public BRException(String message) {
        super(message);
    }

}
